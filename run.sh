#!/bin/sh


set -e

CONF_ROOT="/etc/nginx/conf.d"

mkdir -p "$CONF_ROOT"

for host in $PROXY_HOSTS
do
  CONF_FILE_NAME="${CONF_ROOT}/${host}.conf"
  cp -p "/nginx-site.conf.template" "$CONF_FILE_NAME"

  sed -i 's/%%HOSTNAME%%/'"$host"'/g' "$CONF_FILE_NAME"

  cat "$CONF_FILE_NAME"
done

ls -l "$CONF_ROOT"
nginx -g "daemon off;"
FROM nginx:alpine

COPY nginx-site.conf.template /nginx-site.conf.template
COPY run.sh /run.sh

CMD ["/run.sh"]